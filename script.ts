/**
 * Задание 1 - Напишите функцию сортировки чисел которая принимает на вход массив
 * строк и тип сортировки ASC или DESC, а возвращает отсортированный в соответствии
 * с типом массив
 * (arr: string[], order: Order) => string[]
 * Использовать: enum
 */

enum Order {
    ASC = `ASC`,
    DESC = `DESC`
}

const sortFunc = (arr: string[], order: Order): string[] | void => {
    if (order === Order.ASC) return arr.sort();
    if (order === Order.DESC) return arr.sort().reverse();
}

// console.log(sortFunc([`c`, `a`, `b`], Order.DESC))

/**
 Задание 2 - Напишите функцию которая генерирует пустой объект организации в зависимости

 от типа организационной собственности ИП или ООО
 getOrganization(type) => Organization
 Организация имеет слудющие поля:
 - ИНН
 КПП (только для ООО)
 ОКПО
 Наименование
 Тип
 Использовать: enum
 */

interface OrganizationObject {
    INN: string,
    KPP?: string,
    OKPO: string,
    name: string,
    type: OrganizationTypes
}

enum OrganizationTypes {
    LEGAL_ENTITY = 'legal-entity',
    LIMITED_LIABILITY_COMPANY = `Limited-Liability-Company`
}


function getOrganization(type: OrganizationTypes): OrganizationObject | void {
    if (type === OrganizationTypes.LEGAL_ENTITY) {
        return {
            INN: ``,
            OKPO: ``,
            name: ``,
            type: type
        }
    } else if (type === OrganizationTypes.LIMITED_LIABILITY_COMPANY) {
        return {
            INN: ``,
            KPP: ``,
            OKPO: ``,
            name: ``,
            type: type
        }
    }

}

/**
 * Задание 3 - Напишите тип для описания координат x и y, напишите тип прямоугольник
 * который пересекатся с типом координат и описывает ширину и длину. Опишите тоже
 * самое через интерфейсы. Почему для типов в данном случае нельзя использовать объединение?
 */
type Points = {
    X: number,
    Y: number
}

type Rectangle = {
    width: number,
    height: number,
} & Points;

const rec: Rectangle = {
    X: 4,
    Y: 0,
    width: 200,
    height: 300
}

interface IPoints {
    X: number,
    Y: number
}

interface IRectangle extends IPoints {
    width: number,
    height: number,
}

const rec2: IRectangle = {
    X: 4,
    Y: 0,
    width: 200,
    height: 300
}

/**

 * Задание 4 - Напишите тип для описания данных пришедших с сервера и возможный случай ошибки.
 * Успешный:
 * {
 * success: true,
 * data: { firstName: 'Oleg' }
 * }
 * Произошла ошибка:
 * {
 * success: false,
 * errors: [{message: 'Ошибка доступа'}]
 * }
 */

type MyUser = { firstname: string };
type MyError = { message: string };

type SuccessfulResponse = {
    success: true,
    data: MyUser
}
type ErrorResponse = {
    success: false,
    errors: MyError[]
}
type Response2 = SuccessfulResponse | ErrorResponse;


/**
 * Задание 5 - Напишите функцию которая в зависимости от типа окружения
 * выводит одну из возможных строк: Продакшн, Тестовое окружение, Девелоп,
 * Типы окружений: prod, test, dev
 * Позаботтесь о проверки на полноту.
 *
 * Добавить типы Стейджинг и Найтли. Убедить что в TS появилась ошибка после проверки
 * на полноту из-за необработанных случаев.
 * Типы окружений: staiging, nightly
 *
 * Использовать тип пуcтого множества never
 */

type Option = `production` | `test` | `dev`;

const getStatus = (status: Option): string => {
    if(status === `production`) return `Production`;
    if(status === `test`) return `Test`;
    if(status === `dev`) return `Develop`;
    const check: never = status;
    return check;

}
const res = getStatus(`production`)

/** Задание 7 - Опишите интерфейс BaseEntity:
 * - id (T) - идентификатор пользователя
 * - addedAt (Дата) - время создания сущности
 * - updatedAt (Дата) - время обновления сущности
 * - addedBy (строка) - кто добавил
 * - updatedBy (строка) - кто последний раз обновил
 *
 * Опишите интерфейс User который расширяет тип BaseEntity с типом string
 * и полями:
 * - firstName: string;
 * - lastName: string;
 *
 * Опишите интерфейс Post который расширяет тип BaseEntity с типом number
 * и полями:
 * - title: string;
 * - authorId: number;
 */

interface BaseEntity2<T> {
    id: T,
    addedAt: Date,
    updatedAt: Date,
    addedBy: string,
    updatedBy: string
}

interface User2 extends BaseEntity2<string> {
    firstName: string,
    lastName: string
}

interface Post extends BaseEntity2<number> {
    title: string,
    authorId: number
}

/**

 * Задание 8 - Напишите тип для описания данных пришедших с сервера и возможный случай ошибки
 * для получения одиночной сущности и массива данных.
 * Успешный:
 * {
 * success: true,
 * data: T
 * }
 * Произошла ошибка:
 * {
 * success: false,
 * errors: [{message: 'Ошибка доступа'}]
 * }
 *
 * и
 *
 * Успешный:
 * {
 * success: true,
 * data: T[]
 * }
 * Произошла ошибка:
 * {
 * success: false,
 * errors: [{message: 'Ошибка доступа'}]
 * }
 *
 * Применить обощение для типа User с полями id, firstName, lastName
 *
 * Использовать: дженерики
 */

type ErrorMessage = {
    message: string
}

type SuccessResponse<T> = {
    success: true,
    data: T
}
type FailedResponse = {
    success: false,
    errors: ErrorMessage[]
}

type UserResponse = SuccessResponse<Pick<User, `id`>[]>;

const user: UserResponse = {
    success: true,
    data: [{
        id: ``
    }]
}

/**

 * Задание 9 - Опишите интерфейс пользователя посложнее.

 * Пользователь, как все сущности системы полученные из базы данных имеет мета-свойства базовой
 * сущности BaseEntity:
 * - id (T) - идентификатор пользователя
 * - addedAt (Q) - время создания сущности
 * - updatedAt (Q) - время обновления сущности
 * - addedBy (P) - кто добавил
 * - updatedBy (P) - кто последний раз обновил
 *
 * Свойства интерфейса User + BaseEntity (id: string, даты - ISOString, addedBy и updatedBy типа string ) :
 * - roles (массив типа Role) - список ролей пользователя
 * - firstName (строка) - имя
 * - secondName (строка) - фамилия
 * - middleName (строка, необязательное поле) - среднее имя
 * - isAdmin (логическое, только для чтения) - является ли пользователь администратором
 *
 * Интферфейс Role также имеет все базовые свойства интерфейса BaseEntity (id: number, даты - Date, addedBy и updatedBy типа string ) и свои:
 * - name (строка) - наименование роли
 */


interface BaseEntity<T, Q, P> {
    id: T,
    addedAt: Q,
    updatedAt: Q,
    addedBy: P,
    updatedBy: P
}

interface Role extends BaseEntity<number, Date, string> {
    name: string
}

interface User extends BaseEntity<string, string, string> {
    roles: Role[],
    firstName: string,
    secondName: string,
    middleName?: string,
    readonly isAdmin: boolean
}


